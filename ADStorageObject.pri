android {
    QT += androidextras
}

INCLUDEPATH += \
    StorageObject \
    StorageObject/StorageInjectorFactory \
    StorageObject/StorageInjectorFactory/StorageInjectors

HEADERS += \
    StorageObject/IStorageObject.h \
    StorageObject/IAbstractStorageObject.h \
    StorageObject/StorageInjectorFactory/IStorageInjector.h \
    StorageObject/StorageInjectorFactory/StorageInjectorFactory.h \
    StorageObject/StorageInjectorFactory/StorageInjectors/JSONStorageInjector.h \
    StorageObject/StorageInjectorFactory/StorageInjectorFactorySingleton.h

SOURCES += \
    StorageObject/StorageInjectorFactory/StorageInjectorFactory.cpp \
    StorageObject/StorageInjectorFactory/StorageInjectors/JSONStorageInjector.cpp

android {
    HEADERS += \
        StorageObject/StorageInjectorFactory/StorageInjectors/JSONStorageInjectorObjC.h

    SOURCES += \
        StorageObject/StorageInjectorFactory/StorageInjectors/JSONStorageInjectorObjC.cpp

    OBJECTIVE_HEADERS += \
        StorageObject/StorageInjectorFactory/StorageInjectors/JSONStorageInjectorObjC.h

    OBJECTIVE_SOURCES += \
        StorageObject/StorageInjectorFactory/StorageInjectors/JSONStorageInjectorObjC.mm

}

ios {
    OBJECTIVE_HEADERS += \
        StorageObject/StorageInjectorFactory/StorageInjectors/JSONStorageInjectorObjC.h

    OBJECTIVE_SOURCES += \
        StorageObject/StorageInjectorFactory/StorageInjectors/JSONStorageInjectorObjC.mm
}
