#ifndef CBRIDGE_H
#define CBRIDGE_H

#include <QObject>
#include <QDebug>
#include "MyDataSingleton.h"
#include <QQmlEngine>
#include <QQmlImageProviderBase>
#include <QQuickImageProvider>
#include <QQmlContext>
#include "JSONStorageInjector.h"
#include "StorageInjectorFactorySingleton.h"

class CBridge : public QObject
{
    Q_OBJECT
public:
    explicit CBridge(QObject *parent = 0) {}

    Q_INVOKABLE MyData* getMyData() {
        MyDataSingleton::instance()->load(MyDataSingleton::instance()->sinjJSON);
        qDebug() << "Loaded Data Name:" << MyDataSingleton::instance()->qstrMyName;
        return MyDataSingleton::instance();
    }

    Q_INVOKABLE bool setMyData(MyData* mydata) {
        return mydata->save(MyDataSingleton::instance()->sinjJSON);
    }
};

#endif // CBRIDGE_H
