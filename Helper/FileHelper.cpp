/*
 * This Class is part of the AppDesigners.de Code Library
 *
 * Copyright (C) Aamer Q. Mahmood (support@appdesigners.de)
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Please obtain a legal license to use this product at AppDesigners.de
 *
 * This copy is Licensed for the Project ImpliProjects
 * The License can be validated here:
 * http://AppDesigners.de/Licenses/?Lib=ADClasses&Customer=ImpliProjects
 *
 * THE SOFTWARE/CODE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
*/

#include "FileHelper.hpp"
#include <QFile>
#include <QDir>
#include <QTextStream>
#include <QDebug>
#include <QFileInfo>
#include <QDateTime>
#include <QDirIterator>
#ifdef Q_OS_ANDROID
    #include <QtAndroidExtras/QAndroidJniObject>
    #include <QtAndroidExtras/QAndroidJniEnvironment>
#endif
#ifdef Q_OS_IOS
    #include "NativeIOSBridge.h"
#endif

FileHelper::FileHelper() {}
FileHelper::~FileHelper() {}

void FileHelper::appendToTextFile(QString qstrFileName, QString content)
{
    QFile qfile(FileHelper::getFilePath(qstrFileName));
    qfile.open(QIODevice::Append);
    QTextStream qtxtstream(&qfile);
    qtxtstream << content;
    qfile.close();
}

bool FileHelper::writeToTextFile(QString qstrFileName, QString content)
{
    QString qstrFile = FileHelper::getFilePath(qstrFileName);
    QFile qfile(qstrFile);
    if (qfile.exists())
        qfile.remove();
    int intWriteSuccess = qfile.open(QIODevice::WriteOnly);
    QTextStream qtxtstream(&qfile);
    qtxtstream.setCodec("UTF-8");
    qtxtstream << content;
    qfile.close();
    if (intWriteSuccess == -1) {
        qDebug() << "Failed to write File:" << qstrFile;
        return false;
    } else {
        qDebug() << "Succeded to write File:" << qstrFile;
        return true;
    }
}

QString FileHelper::readFromTextFile(QString qstrFileName)
{
    QString qstrFile = FileHelper::getFilePath(qstrFileName);
    QFile qfile(qstrFile);
    qfile.open(QIODevice::ReadOnly);
    QTextStream in(&qfile);
    in.setCodec("UTF-8");
    QString qstrContent = in.readAll();
    qfile.close();
    qDebug() << "Succeded to read File:" << qstrFile << " Content:" << qstrContent;
    return qstrContent;
}

QString FileHelper::readFromTextFile(QString qstrFileName, QString filePath)
{
    QFile qfile(filePath+"/"+qstrFileName);
    qfile.open(QIODevice::ReadOnly);
    QString qstrContent = qfile.readAll();
    qfile.close();
    return qstrContent;
}

QImage FileHelper::readFromImageFile(QString qstrFileName, QString format) {
    QImage qimg;
    qimg.load(FileHelper::getFilePath(qstrFileName)/*,format.toStdString().c_str()*/);
    bool boolInvalid = qimg.isNull();
    if (boolInvalid) qDebug() << "\tImage:" << qstrFileName << " failed to load";
    return qimg;//(boolInvalid ? qimg : QImage("blank.png"));
}
void FileHelper::writeImageToFile(QString qstrFileName, QImage qimg, QString format) {
    if (FileHelper::fileExists(qstrFileName))
        FileHelper::deleteFile(qstrFileName);
    qimg.save(FileHelper::getFilePath(qstrFileName),format.toStdString().c_str());
}

QByteArray FileHelper::readFileToByteArray(QString qstrFileName)
{
    QFile fContent(FileHelper::getFilePath(qstrFileName));
    fContent.open(QIODevice::ReadOnly);
    return fContent.readAll();
}

bool FileHelper::writeByteArrayToFile(QByteArray baFileContent,const QString qstrFileName)
{
    if (FileHelper::fileExists(qstrFileName))
        FileHelper::deleteFile(qstrFileName);

    QString qstrFile = FileHelper::getFilePath(qstrFileName);
    QFile fContent(qstrFile);
    fContent.open(QIODevice::WriteOnly);
    int intWritten = fContent.write(baFileContent,baFileContent.length());
    bool boolWritten = intWritten != -1;
    if (!boolWritten) {
        qDebug() << "Failed to write File:" << qstrFile << " Open?:" << fContent.isOpen() << " Length:" << baFileContent.length() << " Written:" << intWritten;
    }
    fContent.close();

    return FileHelper::fileExists(qstrFileName);//boolWritten;
}

void FileHelper::writeIntToTextFile(QString qstrFileName, int intVal)
{
    QFile qfile(FileHelper::getFilePath(qstrFileName));
    qfile.open(QIODevice::WriteOnly);
    QTextStream qtxtstream(&qfile);
    qtxtstream << intVal;
    qfile.close();
}
void FileHelper::writeLongToTextFile(QString qstrFileName, long lngVal) {
    QFile qfile(FileHelper::getFilePath(qstrFileName));
    qfile.open(QIODevice::WriteOnly);
    QTextStream qtxtstream(&qfile);
    qtxtstream << lngVal;
    qfile.close();
}

long FileHelper::readLongFromTextFile(QString qstrFileName)
{
    return FileHelper::readFromTextFile(qstrFileName).toLong();
}

int FileHelper::readIntFromTextFile(QString qstrFileName)
{
    return FileHelper::readFromTextFile(qstrFileName).toInt();
}

bool FileHelper::fileExists(QString qstrFileName)
{
    QString qstrFile = FileHelper::getFilePath(qstrFileName);
    QFile qfile(qstrFile);
    bool boolExists = qfile.exists();
    qDebug() << "File:" << qstrFile << " exists:" << boolExists;
    return boolExists;
}

void FileHelper::deleteFile(QString qstrFileName)
{
    if (FileHelper::fileExists(qstrFileName)) {
        QFile qfile(FileHelper::getFilePath(qstrFileName));
        qfile.remove();
    }
}

long FileHelper::fileModifiedAt(QString qstrFileName) {
    if (FileHelper::fileExists(qstrFileName)) {
        QFileInfo qfinfo(FileHelper::getFilePath(qstrFileName));
        QDateTime dtFile = qfinfo.lastModified();
        qDebug() << "FileModifiedAt (4:" << qstrFileName << "):" << dtFile;
        QString qstrFileModified = dtFile.toString("ddmmyyhhmm");
        return qstrFileModified.toLong();
    }
}

QString FileHelper::getFilePath()
{
    #ifdef Q_OS_ANDROID
        QAndroidJniObject jfilePath = QAndroidJniObject::callStaticObjectMethod("android/os/Environment",
                                                                                "getExternalStorageDirectory",
                                                                                "()Ljava/io/File;");
        QAndroidJniObject jstrPath = jfilePath.callObjectMethod("getAbsolutePath", "()Ljava/lang/String;");
        QString dataAbsPath = jstrPath.toString()+"/";

        QAndroidJniEnvironment env;
        if (env->ExceptionCheck()) {
            qDebug() << "ExceptionCheck:true => FAILED to get FilePath!";
            env->ExceptionClear();
        }

        return dataAbsPath;
    #else
        #ifdef Q_OS_IOS
            std::string strFolderPath = NativeIOSBridge::getMainPath();
            QString qstrFolderPath(strFolderPath.c_str());
            return qstrFolderPath;
        #else
            QString qstrTemp = QDir::tempPath() + "/";
            //qDebug() << "Windows Temp:" << qstrTemp;
            return qstrTemp;
        #endif
    #endif
}

QString FileHelper::getFilePath(QString qstrFileName)
{
    return getFilePath()+qstrFileName;
}
QString FileHelper::getAssetFilePath(QString qstrFileName)
{
    return ":/"+qstrFileName;
}

bool FileHelper::copyFile(QString qstrSrcFile, QString qstrTarFile) {
    QFile qfileSrc(qstrSrcFile);
    QFile qfileTar(qstrTarFile);
    if (qfileTar.exists())
        qfileTar.remove();
    return qfileSrc.copy(qstrTarFile);
}

int FileHelper::readIntFromAssetTextFile(QString qstrFileName){
    QFile qfile(FileHelper::getAssetFilePath(qstrFileName));
    qfile.open(QIODevice::ReadOnly);
    QString qstrContent = qfile.readAll();
    qfile.close();
    return qstrContent.toInt();
}
bool FileHelper::cacheFileFromAsset(QString qstrFileName){
    QFile qfile(FileHelper::getAssetFilePath(qstrFileName));
    if (qfile.exists()) {
        bool boolCacheUpdated = qfile.copy(FileHelper::getFilePath(qstrFileName.replace("//","/")));
        if (!boolCacheUpdated) qDebug() << "Failed to Asset File to Cache:" << qstrFileName;
        return boolCacheUpdated;
    } else
        return false;
}
bool FileHelper::cacheImageFileFromAsset(QString qstrFileName){
    return FileHelper::cacheFileFromAsset(qstrFileName);
}
bool FileHelper::assetFileExists(QString qstrFileName){
    if (qstrFileName.endsWith(".log")
        && !qstrFileName.endsWith(".bereich.log"))
            return false;
    QString qstrAssetFile = FileHelper::getAssetFilePath(qstrFileName);
    QFile qfile(qstrAssetFile);
    bool boolExists = qfile.exists();
    qDebug() << "\tAssetFile:" << qstrAssetFile << " exists:" << boolExists;
    return boolExists;
}


int FileHelper::countFiles(QString qstrPath, QString qstrPattern) {
    QString qstrStaticPath = (qstrPath.startsWith("/") ? qstrPath : getFilePath(qstrPath));
    int intCount = 0;

    QStringList listPattern;
    listPattern.append(qstrPattern);

    QDir dirPath(qstrStaticPath);
    dirPath.setFilter(QDir::Files);
    dirPath.setNameFilters(listPattern);

    QDirIterator it(dirPath, QDirIterator::Subdirectories);
    while (it.hasNext()) {
        intCount++;
        it.next();
    }
    return intCount;
}
