/*
 * This Class is part of the AppDesigners.de Code Library
 *
 * Copyright (C) Aamer Q. Mahmood (support@appdesigners.de)
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Please obtain a legal license to use this product at AppDesigners.de
 *
 * This copy is Licensed for the Project ImpliProjects
 * The License can be validated here:
 * http://AppDesigners.de/Licenses/?Lib=ADClasses&Customer=ImpliProjects
 *
 * THE SOFTWARE/CODE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
*/

#include <QString>
#include <QImage>
#include <QObject>
#include <QFileInfo>

#ifndef FILEHELPER_H
#define FILEHELPER_H

class FileHelper : public QObject
{
    Q_OBJECT
public:
    FileHelper();

    Q_INVOKABLE static void appendToTextFile(QString qstrFileName, QString content);
    Q_INVOKABLE static bool writeToTextFile(QString qstrFileName, QString content);
    Q_INVOKABLE static QString readFromTextFile(QString qstrFileName);
    static QString readFromTextFile(QString qstrFileName, QString filePath);

    Q_INVOKABLE static void writeIntToTextFile(QString qstrFileName, int intVal);
    Q_INVOKABLE static int readIntFromTextFile(QString qstrFileName);

    Q_INVOKABLE static void writeLongToTextFile(QString qstrFileName, long lngVal);
    Q_INVOKABLE static long readLongFromTextFile(QString qstrFileName);

    static QImage readFromImageFile(QString qstrFileName, QString format);
    static void writeImageToFile(QString qstrFileName, QImage qimg, QString format);

    static QByteArray readFileToByteArray(QString qstrFileName);
    static bool writeByteArrayToFile(QByteArray baFileContent, QString qstrFileName);

    Q_INVOKABLE static bool fileExists(QString qstrFileName);
    Q_INVOKABLE static void deleteFile(QString qstrFileName);
    static QString getFilePath();
    static QString getFilePath(QString qstrFileName);
    static QString getAssetFilePath(QString qstrFileName);

    static int readIntFromAssetTextFile(QString qstrFileName);
    static bool cacheFileFromAsset(QString qstrFileName);
    static bool cacheImageFileFromAsset(QString qstrFileName);
    static bool assetFileExists(QString qstrFileName);

    static bool copyFile(QString qstrSrcFile, QString qstrTarFile);

    static long fileModifiedAt(QString qstrFileName);
    static int countFiles(QString qstrPath, QString qstrPattern);

    Q_INVOKABLE static bool isImage(QString qstrImageFile) {
        QString qstrImageFileUpper = qstrImageFile.toUpper();
        return (qstrImageFileUpper.endsWith("JPG")
                || qstrImageFileUpper.endsWith("PNG")
                || qstrImageFileUpper.endsWith("GIF"));
    }

    Q_INVOKABLE static qint64 GetFileSize(QString qstrImageFile) {
        QFileInfo fiImage(qstrImageFile);
        return fiImage.size();
    }
    Q_INVOKABLE static QString GetFileName(QString qstrImageFile) {
        QFileInfo fiImage(qstrImageFile);
        return fiImage.fileName();
    }

    Q_INVOKABLE static int GetImageHeight(QString qstrImageFile) {
        QImage qimg(qstrImageFile);
        return qimg.height();
    }
    Q_INVOKABLE static int GetImageWidth(QString qstrImageFile) {
        QImage qimg(qstrImageFile);
        return qimg.width();
    }

    ~FileHelper();
};

#endif // FILEHELPER_H
