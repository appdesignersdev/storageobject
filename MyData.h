#include <QString>
#include <QDebug>
#include <QObject>
#include "IStorageObject.h"

#ifndef MYDATA_H
#define MYDATA_H

class MyData : public IStorageObject {
    Q_OBJECT
    Q_PROPERTY(QString qstrMyId MEMBER qstrMyId)
    Q_PROPERTY(QString qstrMyTel MEMBER qstrMyTel)
    Q_PROPERTY(QString qstrMyMail MEMBER qstrMyMail)
    Q_PROPERTY(QString qstrMyName MEMBER qstrMyName)
    Q_PROPERTY(QString qstrMyAnschrift MEMBER qstrMyAnschrift)
    Q_PROPERTY(double dblMyLat MEMBER dblMyLat)
    Q_PROPERTY(double dblMyLon MEMBER dblMyLon)

public:
    QString qstrMyId = "";
    QString qstrMyTel = "";
    QString qstrMyMail = "";
    QString qstrMyName = "";
    QString qstrMyAnschrift = "";
    double dblMyLat = 0.0;
    double dblMyLon = 0.0;

protected:
    ~MyData() { }
    MyData() { }
};

#endif // MYDATA_H
