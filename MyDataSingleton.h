#include <QDebug>
#include "Singleton.h"
#include "MyData.h"
#include "StorageInjectorFactorySingleton.h"

#ifndef MYDATASINGLETON_H
#define MYDATASINGLETON_H

class MyDataSingleton : public Singleton<MyDataSingleton>, public MyData {
    friend class Singleton <MyDataSingleton>;

private:

public:
    IStorageInjector* sinjJSON;
    bool boolLoadedFlag = false;

    bool saveMyData() {
        bool boolStored = this->save(this->sinjJSON);
        qDebug() << "MaklerData saved:" << boolStored;
        return true;
    }

    void init() {
        qDebug() << "Inited MyData.json Storage Injector";
        #ifdef Q_OS_ANDROID
            this->sinjJSON = StorageInjectorFactorySingleton::CreateStorageInjector("JSONStorageInjector_Android","MyData.json");
        #else
            this->sinjJSON = StorageInjectorFactorySingleton::CreateStorageInjector("JSONStorageInjector_iOS","MyData.json");
        #endif
    }

    virtual bool load(IStorageInjector* storagehandle) {
        this->boolLoadedFlag = IStorageObject::load(storagehandle);
        return this->boolLoadedFlag;
    }

protected:
    ~MyDataSingleton () {
        delete this->sinjJSON;
    }
    MyDataSingleton () {
        init();
    }
};

#endif // MYDATASINGLETON_H
