#ifndef IABSTRACTSTORAGEOBJECT_H
#define IABSTRACTSTORAGEOBJECT_H

#include <QObject>

template <class StorageInjector>
class IAbstractStorageObject : public QObject
{
public:
    virtual bool store(StorageInjector* storagehandle) = 0;
    virtual bool read(StorageInjector* storagehandle) = 0;
    virtual bool save(StorageInjector* storagehandle) = 0;
    virtual bool load(StorageInjector* storagehandle) = 0;
};


#endif // IABSTRACTSTORAGEOBJECT_H
