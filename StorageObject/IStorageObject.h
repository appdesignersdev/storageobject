#include "StorageInjectorFactory/IStorageInjector.h"
#include "IAbstractStorageObject.h"
#include <QMetaProperty>
#include <QDebug>

#ifndef STORAGEOBJECT_H
#define STORAGEOBJECT_H

//Läuft nur ab Qt 5.13 (Qt 5.6 -> Copy Constructor <- QMeta Fehler)

class IStorageObject : public IAbstractStorageObject<IStorageInjector>
{
public:
    IStorageObject() {}
    IStorageObject(const IStorageObject &other) { this->copyconstruct(other); }
    ~IStorageObject() {}

    friend bool operator!=(const IStorageObject& lhs, const IStorageObject& rhs) {
        bool boolEqual = true;
        const QMetaObject* meta = lhs.metaObject();
        for (int i=1; i < meta->propertyCount(); i++) {
            QMetaProperty metaproperty = meta->property(i);
            const char* cptrName = metaproperty.name();
            QString qstrType(metaproperty.typeName());
            QVariant r = rhs.property(cptrName);
            QVariant l = lhs.property(cptrName);
            if (qstrType.compare("QString") != 0)
                boolEqual = boolEqual && (r.toString().compare(l.toString()));
            else if (qstrType.compare("IStorageObject") != 0)
                boolEqual = boolEqual && (rhs.property(metaproperty.name()) == lhs.property(metaproperty.name()));
        }
        return !boolEqual;
    }

    IStorageObject& operator=(const IStorageObject& other) {
        this->copyconstruct(other);
        return *this;
    }

private:
    void copyconstruct(const IStorageObject &other) {
        const QMetaObject* meta = other.metaObject();
        for (int i=0; i < meta->propertyCount(); i++) {
            QMetaProperty metaproperty = meta->property(i);
            this->setProperty(metaproperty.name(),other.property(metaproperty.name()));
        }
    }

public:
    virtual bool store(IStorageInjector* storagehandle) {
        bool boolSuccess = true;
        const QMetaObject* meta = this->metaObject();
        qDebug() << "Store// count:" << meta->propertyCount();
        for (int i=1; i < meta->propertyCount(); ++i) {
            QMetaProperty metaproperty = meta->property(i);
            if (QString(metaproperty.typeName()).compare("IStorageObject") != 0) {
                boolSuccess = boolSuccess && storagehandle->consume(metaproperty.name(),this->property(metaproperty.name()));
            } else {
                QVariant qvobj = this->property(metaproperty.name());
                qDebug() << qvobj.constData();
                IStorageObject sobj = qvobj.value<IStorageObject>();
                qDebug() << QString(metaproperty.name()) << "-> 2consume:" << qvobj << "-> #2consume:" << (sobj.metaObject()->propertyCount()-1);
                boolSuccess = boolSuccess && storagehandle->consume(metaproperty.name(),&sobj);
            }
        }
        return boolSuccess;
    }

    virtual bool save(IStorageInjector* storagehandle) {
        if (storagehandle->init(IStorageInjector::Write) && this->store(storagehandle)) return storagehandle->commit(IStorageInjector::Write);
                                                                                   else return false;
    }

    virtual bool read(IStorageInjector* storagehandle) {
        bool boolSuccess = true;
        const QMetaObject* meta = this->metaObject();
        for (int i=1; i < meta->propertyCount(); ++i) {
            QMetaProperty metaproperty = meta->property(i);
            QVariant qobjValue = storagehandle->get(metaproperty.name());
            boolSuccess = boolSuccess && this->setProperty(metaproperty.name(),qobjValue);
        }
        return boolSuccess;
    }

    virtual bool load(IStorageInjector* storagehandle) {
        if (storagehandle->init(IStorageInjector::Read)) return this->read(storagehandle) && storagehandle->commit(IStorageInjector::Read);
                                                    else return false;
    }
};

Q_DECLARE_METATYPE(IStorageObject)

#endif
