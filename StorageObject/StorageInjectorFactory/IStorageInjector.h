/*
 * IProperty.h gives C++ Property capabilities (For Usage see Example below class)
 *
 * Copyright (C) 2014 Aamer Q. Mahmood (support@appdesigners.de)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "IAbstractStorageObject.h"

#include <QObject>
#include <QVariant>

#ifndef STORAGEINJECTOR_H
#define STORAGEINJECTOR_H

class IStorageInjector : public QObject
{
    Q_OBJECT
public:
    typedef IAbstractStorageObject<IStorageInjector> IStorageObject;
    enum Mode {Read, Write};

    virtual void setFile(QString qstrFileName) = 0;

    virtual bool consume(QString qstrTag, QVariant objVal) = 0;
    virtual QVariant get(QString qstrTag) = 0;

    virtual bool init(Mode mode) = 0;
    virtual bool commit(Mode mode) = 0;
    virtual void begin(QString qstrTag) {}
    virtual void end(QString qstrTag) {}
    virtual long modifiedAt() {}

    virtual bool consume(QString qstrTag, IStorageObject* sobj) {
        this->begin(qstrTag);
        bool boolSuccess = sobj->save(this);
        this->end(qstrTag);
        return boolSuccess;
    }
};

typedef IStorageInjector* (*CreateStorageInjectorFn)(void);

#endif
