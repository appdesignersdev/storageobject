#include "StorageInjectorFactory.h"

#include "JSONStorageInjector.h"
#include "JSONStorageInjectorObjC.h"

StorageInjectorFactory::StorageInjectorFactory()
{
#ifdef Q_OS_ANDROID
    Register("JSONStorageInjector_Android", &JSONStorageInjector::Create);
#else
    #ifdef Q_OS_WIN
        Register("JSONStorageInjector_Windows", &JSONStorageInjector::Create);
    #else
        Register("JSONStorageInjector_iOS", &JSONStorageInjectorObjC::Create);
    #endif
#endif
}

void StorageInjectorFactory::Register(const string &strID, CreateStorageInjectorFn pfnCreate)
{
    m_FactoryMap.insert(pair<string,CreateStorageInjectorFn>(strID,pfnCreate));
}

IStorageInjector* StorageInjectorFactory::CreateStorageInjector(const string &strID, QString qstrFileName)
{
    typename std::map<std::string,CreateStorageInjectorFn>::iterator
        intItemExists = m_FactoryMap.find(strID);
    IStorageInjector* storeinj = intItemExists->second();
    storeinj->setFile(qstrFileName);
    return ( intItemExists != m_FactoryMap.end() ? storeinj : NULL);
}

