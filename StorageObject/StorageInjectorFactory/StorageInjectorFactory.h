#ifndef STORAGEINJECTORFACTORY_H
#define STORAGEINJECTORFACTORY_H

#include "IStorageInjector.h"
#include <QString>

using namespace std;
class StorageInjectorFactory
{
public:
    StorageInjectorFactory();
    StorageInjectorFactory(const StorageInjectorFactory &) { }
    StorageInjectorFactory &operator=(const StorageInjectorFactory &) { return *this; }

    typedef map<string,CreateStorageInjectorFn> FactoryMap;
    FactoryMap m_FactoryMap;
public:
    ~StorageInjectorFactory() { m_FactoryMap.clear(); }

    static StorageInjectorFactory *instance()
    {
        static StorageInjectorFactory instance;
        return &instance;
    }

    void Register(const string &strStorageInjectorID, CreateStorageInjectorFn pfnCreate);
    IStorageInjector *CreateStorageInjector(const string &strStorageInjectorID, QString qstrFileName);
};

#endif
