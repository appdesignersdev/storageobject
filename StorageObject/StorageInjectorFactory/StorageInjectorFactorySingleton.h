#include "Singleton.h"
#include "StorageInjectorFactory.h"

#ifndef STORAGEINJECTORFACTORYSINGLETON_H
#define STORAGEINJECTORFACTORYSINGLETON_H

class StorageInjectorFactorySingleton : public Singleton<StorageInjectorFactorySingleton> {
    friend class Singleton <StorageInjectorFactorySingleton>;
public:
    static IStorageInjector* CreateStorageInjector(std::string strID, QString qstrFile) {
        StorageInjectorFactory* storeinjfactory = new StorageInjectorFactory();
        return storeinjfactory->CreateStorageInjector(strID,qstrFile);
    }
};

#endif // STORAGEINJECTORFACTORYSINGLETON_H
