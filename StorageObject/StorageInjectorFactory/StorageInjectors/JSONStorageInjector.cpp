#include "JSONStorageInjector.h"
#include <QDir>
#include <QDebug>
#include "Helper/FileHelper.hpp"

JSONStorageInjector::JSONStorageInjector() {}
JSONStorageInjector::JSONStorageInjector(QString qstrFileName){
    this->setFile(qstrFileName);
}
void JSONStorageInjector::setFile(QString qstrFile) {
    this->_qstrFileName = qstrFile;
}

bool JSONStorageInjector::consume(QString qstrTag, QVariant objVal) {
    this->_qstrJSON += "\t\"" + qstrTag + "\": \"" + objVal.toString().toUtf8().toBase64() + "\",";
    return true;
}

bool JSONStorageInjector::consume(QString qstrTag, IStorageObject* sobj) { return false; }

QVariant JSONStorageInjector::get(QString qstrTag) {
    QString qstrValB64 = this->_json[qstrTag].toString();
    QByteArray baVal64;
    baVal64.append(qstrValB64);
    QString qstrVal = QByteArray::fromBase64(baVal64);
    QVariant qvar = QVariant::fromValue(qstrVal);
    return qvar;
}

bool JSONStorageInjector::init(Mode mode) {
    if (mode == IStorageInjector::Write) {
        this->_qstrJSON = " { \n";
        return true;
    } else {
        if (FileHelper::fileExists(this->_qstrFileName)) {
            QByteArray baJSON = FileHelper::readFileToByteArray(this->_qstrFileName);
            QJsonDocument jsondoc = QJsonDocument::fromJson(baJSON);
            this->_json = jsondoc.object();
            return true;
        } else {
            qDebug() << "File not found:" << FileHelper::getFilePath(this->_qstrFileName);
            return false;
        }
    }
}

bool JSONStorageInjector::commit(Mode mode) {
    if (mode == IStorageInjector::Read) {
        return true;
    } else {
        if (this->_qstrJSON.endsWith(",")) this->_qstrJSON.chop(1);
        this->_qstrJSON += " }";
        qDebug() << "writing to:" << FileHelper::getFilePath(this->_qstrFileName);
        return FileHelper::writeToTextFile(this->_qstrFileName,this->_qstrJSON);
    }
}

long JSONStorageInjector::modifiedAt() {
    return FileHelper::fileModifiedAt(this->_qstrFileName);
}
