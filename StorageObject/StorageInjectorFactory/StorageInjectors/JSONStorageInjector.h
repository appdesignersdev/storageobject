#include "IStorageInjector.h"
#include <QFile>
#include <QJsonObject>
#include <QJsonDocument>

#ifndef JSONStorageInjector_hpp
#define JSONStorageInjector_hpp

class JSONStorageInjector : public IStorageInjector
{
public:
    JSONStorageInjector();
    JSONStorageInjector(QString strFile);
    void setFile(QString qstrFile);
    static IStorageInjector * Create() { return new JSONStorageInjector(); }
    bool consume(QString qstrTag, QVariant objVal);
    bool consume(QString qstrTag, IStorageObject* sobj);
    QVariant get(QString qstrTag);
    bool init(Mode mode);
    bool commit(Mode mode);
    long modifiedAt();
protected:
    QString _qstrConfigFile = "";
    static QByteArray loadFileAsByteArray(QFile &qfileSrc);
private:
    QString _qstrFileName = "";
    QString _qstrJSON = "";
    QJsonObject _json;
};

#endif /* JSONStorageInjector_hpp */
