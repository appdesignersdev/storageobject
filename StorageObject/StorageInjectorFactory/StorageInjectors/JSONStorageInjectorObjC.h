//
//  JSONStorageInjectorObjC.hpp
//
//  Created by aqm on 08.02.17.
//
//

#include "IStorageInjector.h"
#include <QFile>

#ifndef JSONStorageInjectorObjC_hpp
#define JSONStorageInjectorObjC_hpp

class JSONStorageInjectorObjC : public IStorageInjector
{
public:    
    JSONStorageInjectorObjC();
    JSONStorageInjectorObjC(QString strFile);
    void setFile(QString qstrFile);
    static IStorageInjector * Create() { return new JSONStorageInjectorObjC(); }
    bool consume(QString qstrTag, QVariant objVal);
    bool consume(QString qstrTag, IStorageObject* sobj);
    QVariant get(QString qstrTag);
    bool init(Mode mode);
    bool commit(Mode mode);
protected:
    QString _qstrConfigFile = "";
    static QByteArray loadFileAsByteArray(QFile &qfileSrc);
};

#endif /* JSONStorageInjectorObjC_hpp */
