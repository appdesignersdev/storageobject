package de.appdesigners.staticobjexample;

import android.content.Context;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.ComponentName;
import android.os.Bundle;
import android.util.Log;
import java.lang.StringBuffer;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.content.Intent;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;

import android.Manifest;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.content.pm.PackageManager;

public class StaticObjExampleApp extends org.qtproject.qt5.android.bindings.QtActivity
{
    private static StaticObjExampleApp m_instance;

    public StaticObjExampleApp()
    {
        Log.d("StaticObjExampleApp", "init ...");
        m_instance = this;
    }
    @Override
    public void onNewIntent(Intent intent) {
        Log.d("StaticObjExampleApp", "onNewIntent()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("StaticObjExampleApp", "onResume()");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("StaticObjExampleApp", "onCreate()");
        init();
    }

    private int init() {
        Log.d("StaticObjExampleApp", "init()");
        initPermissions();
        return 1;
    }

    private int intPermissionCount = 1;

    private void initPermissions() {
        if (PermissionChecker.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
            },7770+intPermissionCount);
        }
    }

    private boolean isPermissionRequestValid(int intRequestCode) {
        return (intRequestCode > 7770 && intRequestCode < 7780);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (isPermissionRequestValid(requestCode)) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d("StaticObjExampleApp","Permission "+concatStringArray(permissions,",")+" Granted.");
            } else {
                Log.e("StaticObjExampleApp","Permission "+concatStringArray(permissions,",")+" NOT Granted.");
            }
        }
    }

    /*****/

    public static String GetMainPath() {
        return Environment.getExternalStorageDirectory().getAbsolutePath();
    }

    public static String GetTempPath() {
        return m_instance.getCacheDir().getAbsolutePath();
    }

    public static int GetStatusBarHeight() {
        return m_instance.getStatusBarHeight();
    }

    public int getStatusBarHeight() {
           int result = 0;
                int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
                if (resourceId > 0) {
                    result = getResources().getDimensionPixelSize(resourceId);
                }
                return result;
    }

    // Helper


    private String concatStringArray(String[] stra, String strDelimiter) {
        String str ="";
        for(int i=0;i<stra.length;i++) {
            if (i != 0) str += strDelimiter;
            str+=stra[i];
        }
        return str;
    }


}
