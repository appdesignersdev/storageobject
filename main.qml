import QtQuick 2.13
import QtQuick.Window 2.13

Window {
    visible: true
    width: 320
    height: 480
    title: qsTr("Storage Object JSON Example")
    Rectangle {
        id: viewMain
        anchors.fill: parent
        property variant mydata

        function load() {
            viewMain.mydata = cbridge.getMyData()
            console.log("qstrMyName:"+viewMain.mydata.qstrMyName)
            txtName.text = viewMain.mydata.qstrMyName
        }

        TextEdit {
            id: txtName
            anchors.centerIn: parent
            height: 20
            width: 200
            Rectangle {
                border.color: "black"
            }
        }
        Text {
            text: "Name:"
            anchors.bottom: txtName.top
            anchors.bottomMargin: 10
            anchors.left: txtName.left
            anchors.right: txtName.right
            horizontalAlignment: Text.AlignHCenter
        }
        Rectangle {
            id: btnLoad
            color: "white"
            border.color: "blue"
            anchors.left: txtName.left
            anchors.right: txtName.right
            anchors.top: txtName.bottom
            anchors.topMargin: 10
            height: 25
            radius: 3
            Text {
                anchors.centerIn: parent
                text: "Load"
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    viewMain.load()
                }
            }
        }
        Rectangle {
            id: btnSave
            color: "white"
            border.color: "blue"
            anchors.left: txtName.left
            anchors.right: txtName.right
            anchors.top: btnLoad.bottom
            anchors.topMargin: 10
            height: 25
            radius: 3
            Text {
                anchors.centerIn: parent
                text: "Save"
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log(viewMain.mydata.qstrMyName)
                    cbridge.setMyData(viewMain.mydata)
                }
            }
        }
    }
}
